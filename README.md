## Broad Interview Coding Assignment

#### Raaid Arshad

I decided to do the assignment in Python using a Jupyter Notebook,
as I think it is a very clean and readable way to show both my thoughts and my work.

To run the project, I have assumed a Linux environment (it worked on my Mac too)
with sudo privileges, as well as Python3.

- Clone the repo
- You might need to install venv on Linux: ```sudo apt-get install python3-venv```
- Run the deployment script:```bash deploy.sh```
- The Jupyter Notebook should open in the browser. If not, go to
http://localhost:8888/notebooks/BroadInstituteCodingAssignment.ipynb

The notebook should already have the right kernel selected and have all outputs visible
(aka all the code has been run).

If you'd like to run all the code for yourself:
- Make sure the right kernel is selected: 
From the menu options, find "Kernel" -> "Change kernel" -> ".venv"
- To see everything run, go to "Kernel" -> "Restart & Run All", or select "Restart"
and run the cells sequentially yourself.

I hope a bash script and my assumptions are okay for this! I figured a Docker container
might be overkill, though it would likely help with any "but it works on my machine" issues.