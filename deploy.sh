#!/usr/bin/env bash

# install virtual environment (assuming python3)
python3 -m venv venv
# activate the virtualenv
source venv/bin/activate
# install the python requirements
pip install -r requirements.txt
# create the ipython kernel from these requirements
sudo ipython kernel install --user --name=.venv
# start the notebook
jupyter notebook BroadInstituteCodingAssignment.ipynb